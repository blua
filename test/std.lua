local	coro
local	putq={}
local	putqhead,putqtail=1,1

-- match a token
local	function filter(get,put)

	local d = {}
	local current

	-- move to next token
	local	function tnext()
		--print("inserted", current and current[1], current and current[2], current and current[3])
		if (current) then
			table.insert(d,current)
		end
		current = {get()}
		
		return current
	end

	-- return true if current token matches, in case it does move to next token
	local	function tmatch(t)
		
		if current[2] == t then
			
			tnext()
			return true
		end
		return false
	end

	local	function tflush()
		
		for _,v in ipairs(d) do
			put(unpack(v))
		end
		d = {}
	end

	while true do
		tnext()
		if tmatch "(" or tmatch "=" then
			local ln = current[1]
			while  tmatch "<string>" or tmatch "<name>" do
				if current[1] == ln and (current[2] == "<name>" or current[2] == "<string>") then
					table.insert(d,{current[1],".."})
				else
					break
				end
			end
			tnext()
		end
		tflush()
	end
end
function tokenf(ln,tok,val)
	return ln,tok,val
end
function tokenf(ln,tok,val)
	if not coro then
		coro = coroutine.create(filter)
		-- this will get stuck in the getter
		coroutine.resume(coro,
			function() return coroutine.yield() end,
			function(l,t,v)
				putq[putqhead]={l,t,v}
				putqhead=$+1
			end)
	end
	local v=coroutine.resume(coro,ln,tok,val)
	-- now it yields in the getter again
	local pt
	pt = putq[putqtail]
	putq[putqtail] = nil
	if pt then putqtail=$+1 else return end
	
	return unpack(pt)
end


