/*
 * (c) kt 2008
 * based on lhf's code, however we're crashproof (aka no dangling LexState)
 */

#define DBG(fmt,...) { if (fmt[0] > '1') { fprintf(stdout, fmt, ...); fprintf(stdout, "\n"); } };

static	int	llex_filter(LexState *ls, SemInfo *seminfo)
{
	lua_State *L = ls->L;
	int i,c = llex(ls, seminfo);
	const char *token;

	lua_getglobal(L,"tokenf");
	if (lua_isnil(L, -1)) {
		lua_pop(L, 1);
		return c;
	}

	/* prepare func args */
	lua_pushinteger(L, ls->linenumber);
	if (c < FIRST_RESERVED) {
		char s = c;
		lua_pushlstring(L, &s, 1);
	} else lua_pushstring(L, luaX_tokens[c-FIRST_RESERVED]);
	if (c == TK_NAME || c == TK_STRING)
		lua_pushlstring(L, getstr(seminfo->ts), seminfo->ts->tsv.len);
	else if (c == TK_NUMBER)
		lua_pushnumber(L, seminfo->r);
	else lua_pushnil(L);

	/* call the user function */
	lua_call(L, 3, 3);

	token = lua_tostring(L, -2);
	if (!token) {
		lua_pop(L, 3);
		return llex_filter(ls, seminfo);
	}

	ls->linenumber = lua_tointeger(L, -3);
	/* if <eof> token is returned, indicate it */
	if (!strcmp(token, "<eof>")) {
		lua_pop(L, 3);
		return TK_EOS;
	}
	for (i = 0; luaX_tokens[i]; i++) {
		if (!strcmp(token, luaX_tokens[i])) {
			c = i+FIRST_RESERVED;
			break;
		}
	}

	/* single char arbitrary token */
	if (!luaX_tokens[i]) c=token[0];
	if (c == TK_NAME || c == TK_STRING) {
		size_t l;
		const char *s = lua_tolstring(L, -1, &l);
		seminfo->ts = s ? luaS_newlstr(L, s, l) : luaS_newliteral(L, "?");
	}
	if (c == TK_NUMBER)
		seminfo->r = lua_tonumber(L, -1);
	return c;
}
#define llex llex_filter
